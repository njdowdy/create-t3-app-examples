import { trpc } from "../utils/trpc";
import { useState } from "react";

export function TaxonSearchBar() {
  const [queryString, setQueryString] = useState({});

  //   const { data } = trpc.species.list.useQuery({}); // list  all
  // const { data } = trpc.species.byName.useQuery({ name: name }); // search on name
  const { data } = trpc.species.byKey.useQuery({}); // search on multi

  const handleSearchChange = (e) => {
    let lowerCase = e.target.value.toLowerCase();
    if (lowerCase.includes(":")) {
      // do field validation here
      lowerCase = lowerCase.replace(/,|:\s*$/, "");
      let j = JSON.parse(
        '{"' + lowerCase.replace(/,/g, '","').replace(/:/g, '":"') + '"}'
      );
      setQueryString(j);
    }
  };

  const handleKeyDown = (e) => {
    e.key === "Enter" && console.log(`Query: ${search}`);
    console.log();
  };

  return (
    <>
      <div className="mt-6">
        <input
          placeholder="Search..."
          type="text"
          id="default-input"
          onChange={handleSearchChange}
          onKeyDown={handleKeyDown}
          className="text-med block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-black focus:border-blue-500 focus:ring-blue-500"
        ></input>
      </div>
      <div>{data && JSON.stringify(data)}</div>
    </>
  );
}

export default TaxonSearchBar;
