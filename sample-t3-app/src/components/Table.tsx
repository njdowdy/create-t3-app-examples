import { MouseEventHandler, useCallback, useState } from "react";

function Table({ data }: { data: Data }) {
  const headers: { key: string; label: string }[] = [
    { key: "id", label: "ID" },
    { key: "genus", label: "Genus" },
    { key: "species", label: "Species" },
    { key: "familyId", label: "Family" },
    { key: "createdAt", label: "Created" },
    { key: "updatedAt", label: "Last Update" },
  ];

  return (
    <div className="my-5 flex flex-col rounded-xl bg-white">
      <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
          <div className="overflow-hidden">
            <table className="min-w-full">
              <thead className="border-b">
                <tr>
                  {headers.map((row) => {
                    return (
                      <th
                        key={row.key}
                        scope="col"
                        className="px-6 py-4 text-left text-sm font-medium text-gray-900"
                      >
                        {row.label}{" "}
                      </th>
                    );
                  })}
                </tr>
              </thead>

              <tbody>
                {data?.map((data) => {
                  return (
                    <tr key={data.id} className="border-b">
                      <td className="whitespace-nowrap px-6 py-4 text-sm font-medium text-gray-900">
                        {data.id}
                      </td>
                      <td className="whitespace-nowrap px-6 py-4 text-sm font-light text-gray-900">
                        {data.genus}
                      </td>
                      <td className="whitespace-nowrap px-6 py-4 text-sm font-light text-gray-900">
                        {data.species}
                      </td>
                      <td className="whitespace-nowrap px-6 py-4 text-sm font-light text-gray-900">
                        {data.familyId}
                      </td>
                      <td className="whitespace-nowrap px-6 py-4 text-sm font-light text-gray-900">
                        {data.createdAt}
                      </td>
                      <td className="whitespace-nowrap px-6 py-4 text-sm font-light text-gray-900">
                        {data.updatedAt}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Table;
