import { type NextPage } from "next";
import Head from "next/head";
import Link from "next/link";
import { signIn, signOut, useSession } from "next-auth/react";
import { useState } from "react";
import TaxonSearchBar from "../components/TaxonSearchBar";
// import SortableTable from "../components/SortableTable";
import { trpc } from "../utils/trpc";

import data1 from "../../data1.json";
import data2 from "../../data2.json";

const Home: NextPage = () => {
  const [search, setSearch] = useState("");
  const [dataSource, setDataSource] = useState(data1);

  return (
    <>
      <Head>
        <title>Taxon Search</title>
        <meta name="description" content="Generated by create-t3-app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="flex min-h-screen flex-col items-center justify-center bg-gradient-to-b from-[#2e026d] to-[#15162c]">
        <div className="text-6xl text-slate-50">
          <h1>Taxon Search</h1>
        </div>
        <TaxonSearchBar />
        <div>
          {/* <SortableTable data={dataSource} /> */}
          {/* <Table data={data} /> */}
        </div>
        <div className="flex flex-col items-center gap-2">
          <AuthShowcase />
        </div>
      </main>
    </>
  );
};

export default Home;

const AuthShowcase: React.FC = () => {
  const { data: sessionData } = useSession();

  const { data: secretMessage } = trpc.auth.getSecretMessage.useQuery(
    undefined, // no input
    { enabled: sessionData?.user !== undefined }
  );

  return (
    <div className="flex flex-col items-center justify-center gap-4">
      <p className="text-center text-2xl text-white">
        {sessionData && <span>Logged in as {sessionData.user?.name}</span>}
        {secretMessage && <span> - {secretMessage}</span>}
      </p>
      <button
        className="rounded-full bg-white/10 px-10 py-3 font-semibold text-white no-underline transition hover:bg-white/20"
        onClick={sessionData ? () => signOut() : () => signIn()} // wrap these in new pages to style
      >
        {sessionData ? "Sign out" : "Sign in"}
      </button>
    </div>
  );
};
