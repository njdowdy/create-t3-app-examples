import { router } from "../trpc";
import { authRouter } from "./auth";
import { speciesRouter } from "./species";

export const appRouter = router({
  species: speciesRouter,
  auth: authRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
