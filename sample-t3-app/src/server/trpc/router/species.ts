import { z } from "zod";
import { router, publicProcedure } from "../trpc";

// const defaultSpeciesSelect = Prisma.validator<Prisma.SpeciesSelect>()({
//   id: true,
//   species: true,
//   createdAt: true,
//   updatedAt: true,
// });

export const speciesRouter = router({
  list: publicProcedure.query(async ({ ctx }) => {
    // destructure context
    const { prisma } = ctx; // prisma comes from context

    const species = await prisma.species.findMany({
      orderBy: [
        {
          createdAt: "desc",
        },
      ],
      // other critera!
    });

    return {
      species,
    };
  }),
  byName: publicProcedure
    .input(
      z.object({
        name: z.string().nullish(), // just for demo
      })
    )
    .query(async ({ ctx, input }) => {
      // destructure context
      const { prisma } = ctx; // prisma comes from context
      const { name } = input;
      const species = await prisma.species.findMany({
        orderBy: [
          {
            createdAt: "desc",
          },
        ],
        where: {
          species: name,
        },
        // other critera!
      });

      return {
        species,
      };
    }),
  byKey: publicProcedure
    .input(
      z.object({
        name: z.string().nullish(), // just for demo
        genus: z.string().nullish(),
        family: z.string().nullish(),
        status: z.boolean().nullish(),
      })
    )
    .query(async ({ ctx, input }) => {
      // destructure context
      const { prisma } = ctx; // prisma comes from context
      const { name, genus, family, status } = input;
      // const species = await prisma.species.findMany({ //querying the species table, ultimately
      //   orderBy: [
      //     {
      //       createdAt: "desc",
      //     },
      //   ],
      //   where: {
      //     genus: {
      //       family: {
      //         family: "Euteliidae",
      //       },
      //     },
      //     status: true,
      //   },
      const genusRes = await prisma.genus.findMany({
        //querying the genus table, ultimately
        // TODO: how to query a joined version of these data tables?
        orderBy: [
          {
            createdAt: "desc",
          },
        ],
        where: {
          family: {
            family: "Euteliidae",
          },
          status: true,
        },
      });

      return {
        genusRes,
      };
    }),
});
